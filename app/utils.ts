import * as crypto from 'node:crypto'


export const genRef = () => {
  const array = new Uint8Array(20);
  crypto.webcrypto.getRandomValues(array);
  return "0x" + Array.from(array).map(b => b.toString(16).padStart(2, "0")).join("");
}
