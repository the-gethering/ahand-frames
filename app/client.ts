import { createPublicClient, http } from 'viem'
import { mainnet } from 'viem/chains'

import { NEXT_CHAIN } from './config'
 

export const client = createPublicClient({ 
  chain: NEXT_CHAIN,
  transport: http()
})
