import { FrameRequest, getFrameMessage } from '@coinbase/onchainkit'
import { NextRequest, NextResponse } from 'next/server'

import { encodeFunctionData, parseEther } from 'viem'

import { aHandABI } from '../../abi'
import { genRef } from '../../utils'

import { NEXT_CHAIN, NEXT_CONTRACT } from '../../config'


async function getResponse(req: NextRequest): Promise<NextResponse | Response> {
  const body: FrameRequest = await req.json()

  const { isValid, message } = await getFrameMessage(body)

  if (!isValid) {
    return new NextResponse('Message not valid', { status: 500 })
  }

  const data = encodeFunctionData({
    abi: aHandABI,
    functionName: '',
    args: [message.input || '', genRef()],
  })

  const txData = {
    chainId: `eip155:${NEXT_CHAIN.id}`,
    method: 'eth_sendTransaction',
    params: {
      abi: [],
      data,
      to: NEXT_CONTRACT,
      value: parseEther('0.00001').toString(),
    },
  }

  return NextResponse.json(txData)
}

export async function POST(req: NextRequest): Promise<Response> {
  return getResponse(req)
}

export const dynamic = 'force-dynamic'
