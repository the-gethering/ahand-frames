import { FrameRequest, getFrameMessage, getFrameHtmlResponse } from '@coinbase/onchainkit'
import { NextRequest, NextResponse } from 'next/server'

import { NEXT_PUBLIC_URL } from '../../config'


async function getResponse(req: NextRequest): Promise<NextResponse> {
  const body: FrameRequest = await req.json()

  const { isValid, message } = await getFrameMessage(body)

  if (!isValid) {
    return new NextResponse('Message not valid', { status: 500 })
  }

  let state = {
    page: 0,
  }

  try {
    state = JSON.parse(decodeURIComponent(message.state?.serialized))
  } catch (e) {
    console.error(e)
  }

  if (message?.button === 3) {
    return NextResponse.redirect(
      'https://ahand.in',
      { status: 302 },
    )
  }

  return new NextResponse(
    getFrameHtmlResponse({
      buttons: [
        {
          label: `State: ${state?.page || 0}`,
        },
        {
          action: 'link',
          label: 'aHand X',
          target: 'https://twitter.com/ahand_in',
        },
        {
          action: 'post_redirect',
          label: 'aHand',
        },
      ],
      image: {
        src: `${NEXT_PUBLIC_URL}/ahand.png`,
      },
      postUrl: `${NEXT_PUBLIC_URL}/api/frame`,
      state: {
        page: state?.page + 1,
        time: new Date().toISOString(),
      },
    }),
  )
}

export async function POST(req: NextRequest): Promise<Response> {
  return getResponse(req)
}

export const dynamic = 'force-dynamic'
