import { FrameRequest, getFrameMessage, getFrameHtmlResponse } from '@coinbase/onchainkit'
import { NextRequest, NextResponse } from 'next/server'

import { encodeFunctionData, parseEther } from 'viem'

import { client } from '../../client'
import { aHandABI } from '../../abi'

import { NEXT_PUBLIC_URL, NEXT_CONTRACT, NEXT_EXPLORER_URL, NEXT_AHAND_URL } from '../../config'


async function getResponse(req: NextRequest): Promise<NextResponse> {
  const body: FrameRequest = await req.json()

  const { isValid } = await getFrameMessage(body)

  if (!isValid) {
    return new NextResponse('Message not valid', { status: 500 })
  }

  const logsPromise = new Promise<any>((resolve, reject) => {
    const timeoutId = setTimeout(() => {
      reject(new Error('Timeout waiting for logs'));
      unwatch();
    }, 4000);

    const unwatch = client.watchContractEvent({
      address: NEXT_CONTRACT,
      abi: aHandABI,
      eventName: 'Raised',
      onLogs: (logs) => {
        resolve(logs)
        unwatch()
      },
    })
  })

  let logs

  try {
    logs = await logsPromise;
    console.log(logs);
  } catch (error) {
    console.error(error);
  }

  const hand = ''
  const ref = ''

  return new NextResponse(
    getFrameHtmlResponse({
      buttons: [
        {
          action: 'link',
          label: 'Cast',
          target: 'https://warpcast.com',
        },
        {
          action: 'link',
          label: 'Share',
          target: `${NEXT_AHAND_URL}/hand/${hand}/${ref}/share`,
        },
        {
          action: 'link',
          label: 'TX',
          target: `${NEXT_EXPLORER_URL}/tx/${body?.untrustedData?.transactionId}`,
        },
      ],
      image: {
        src: `${NEXT_PUBLIC_URL}/ahand.png`,
      },
    }),
  )
}

export async function POST(req: NextRequest): Promise<Response> {
  return getResponse(req)
}

export const dynamic = 'force-dynamic'
