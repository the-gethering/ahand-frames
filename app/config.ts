import * as chains from 'viem/chains'

export const NEXT_CHAIN = process.env.NODE_ENV == 'development' ? chains.baseSepolia : chains.base
export const NEXT_PUBLIC_URL = process.env.NODE_ENV == 'development' ? 'https://welcome-primate-specially.ngrok-free.app' : 'https://frames.ahand.in'
export const NEXT_CONTRACT = process.env.NODE_ENV == 'development' ? '0x4A8936aaD950da2B02A6DF6DA7B0114B884546e5' : '0x5253bdb502be3d85c3932292accaf16233058e7f'
export const NEXT_EXPLORER_URL = process.env.NODE_ENV == 'development' ? 'https://sepolia.basescan.org' : 'https://basescan.org'
export const NEXT_AHAND_URL = process.env.NODE_ENV == 'development' ? 'https://localhost:5173' : 'https://ahand.in'
