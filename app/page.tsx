import { getFrameMetadata } from '@coinbase/onchainkit'

import type { Metadata } from 'next'

import { NEXT_PUBLIC_URL } from './config'


const frameMetadata = getFrameMetadata({
  image: {
    src: `${NEXT_PUBLIC_URL}/ahand.png`,
    aspectRatio: '1.91:1',
  },
  input: {
    text: 'Problem',
  },
  buttons: [
    {
      label: '✋ Raise',
      action: 'tx',
      target: `${NEXT_PUBLIC_URL}/api/tx`,
      postUrl: `${NEXT_PUBLIC_URL}/api/tx-success`,
    }
  ],
  postUrl: `${NEXT_PUBLIC_URL}/api/frame`,
})


export const metadata: Metadata = {
  title: 'aHand',
  description: 'A hand is near. Shake it!',
  openGraph: {
    title: 'aHand',
    description: 'A hand is near. Shake it!',
    images: [`${NEXT_PUBLIC_URL}/ahand.png`],
  },
  other: {
    ...frameMetadata,
  },
}

export default function Page() {
  return <> 
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', minHeight: '100vh'}}>
      <h1 style={{textAlign: 'center'}}>
        a<span style={{display: 'inline-block', fontSize: '1.5em', marginRight: '5px'}}>🙌</span>and Frames
      </h1>
      <a href='https://gitlab.com/the-gethering/ahand-frames' style={{display: 'block', textAlign: 'center'}}>
        source code
      </a>
    </div>
  </>
}
